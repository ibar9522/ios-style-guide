# Apple style guide

- Link to document: https://google.github.io/swift/

# Дополнение к основному гайду

## Переносы на новую линию

- Если параметров больше чем 3, то переносить их на новую линию (желательно всегда нажимать ctrl + i, чтобы "отшлифовать")
- Правильный пример: 
    ```swift
    func exampleFunc(
        par1: Any,
        par2: Any,
        par3: Any
        ) {
            ...
        }
    ```
- Плохой пример: 
    ```swift
    func wrongExample(par1: Any, par2: Any, par3: Any, par4: Any) {
        ...
    }
    ```

## Цифровые значения

- Все цифровые тайпы кроме Int должны иметь десятичную часть
- Правильный пример:
    ```swift
    let intValue: Int = 0

    let doubleValue: Double = 0.0

    let floatValue: CGFloat = 35.0
    ```
- Плохой пример:
    ```swift
    let intValue: Int = 0

    let doubleValue: Double = 0

    let floatValue: CGFloat = 35
    ```

## Guard statement

- Если return body в guard короткая и умещается в одну линии, то не нужно перемещать return body на новую строку
- Правильный пример:
    ```swift
    guard let value = anotherValue else { return true }

    guard let value = anotherValue else { return a + b }
    ```
- Плохой пример:
    ```swift
    guard let value = anotherValue {
        return true
    }

    guard let value = anotherValue {
        return a + b
    }
    ```

## Группировка обьектов

- Группировать var, let, lazy var и weak var
- Правильный пример:
    ```swift
    weak var delegate: Delegate?

    let a = 1
    let b = "b"
    let trianlgeHeight = 245.0

    var name = "name"
    var number = 7

    lazy var layout = UICollectionViewFlowLayout()
    lazy var value = someValue
    ```
- Плохой пример:
    ```swift
    lazy var layout = UICollectionViewFlowLayout()
    lazy var value = someValue
    var name = "name"
    
    let a = 1
    var number = 7
    weak var delegate: Delegate?
    let b = "b"
    let trianlgeHeight = 245.0
    ```
